package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementBankDto;
import ma.octo.assignement.dto.VirementBankDto;
import ma.octo.assignement.repository.CompteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class TransactionBankMapperTest {

    CompteRepository compteRepository = Mockito.mock(CompteRepository.class);
    TransactionMapper transactionMapper = new TransactionMapper(compteRepository);
    Compte compte = new Compte();

    @BeforeEach
    public void setUp() {
        compte.setNrCompte("myNr");
        when(compteRepository.findByNrCompte(eq("myNr"))).thenReturn(compte);
    }

    @Test
    public void map_shouldReturnNullWhenGivenNull() {
        assertNull(transactionMapper.map((Virement) null));
        assertNull(transactionMapper.map((Versement) null));
        assertNull(transactionMapper.map((VirementBankDto) null));
        assertNull(transactionMapper.map((VersementBankDto) null));
    }

    @Test
    public void mapToVirmentDto_ok() {
        Virement virement = new Virement();
        virement.setCompteBeneficiaire(compte);
        virement.setCompteEmetteur(compte);
        VirementBankDto virementDto = transactionMapper.map(virement);
        assertEquals(virement.getCompteEmetteur().getNrCompte(), virementDto.getNrCompteEmetteur());
        assertEquals(virement.getCompteBeneficiaire().getNrCompte(), virementDto.getNrCompteBeneficiaire());
        assertEquals(virement.getDateExecution(), virementDto.getDateExecution());
        assertEquals(virement.getMontant(), virementDto.getMontant());
        assertEquals(virement.getMotif(), virementDto.getMotif());
    }

    @Test
    public void mapToVersementDto_ok() {
        Versement versement = new Versement();
        versement.setCompteBeneficiaire(compte);

        VersementBankDto versementDto = transactionMapper.map(versement);
        assertEquals(versement.getNomPrenomEmetteur(), versementDto.getNomPrenomEmetteur());
        assertEquals(versement.getCompteBeneficiaire().getNrCompte(), versementDto.getNrCompteBeneficiaire());
        assertEquals(versement.getDateExecution(), versementDto.getDateExecution());
        assertEquals(versement.getMontant(), versementDto.getMontant());
        assertEquals(versement.getMotif(), versementDto.getMotif());
    }

    @Test
    void mapToVirment_ok() {
        VirementBankDto virementDto = new VirementBankDto();
        virementDto.setNrCompteEmetteur("myNr");
        virementDto.setNrCompteBeneficiaire("myNr");
        Virement virement = transactionMapper.map(virementDto);
        assertEquals(virement.getCompteEmetteur().getNrCompte(), virementDto.getNrCompteEmetteur());
        assertEquals(virement.getCompteBeneficiaire().getNrCompte(), virementDto.getNrCompteBeneficiaire());
        assertEquals(virement.getDateExecution(), virementDto.getDateExecution());
        assertEquals(virement.getMontant(), virementDto.getMontant());
        assertEquals(virement.getMotif(), virementDto.getMotif());
    }

    @Test
    public void mapToVersement_ok() {
        VersementBankDto versementDto = new VersementBankDto();
        versementDto.setNrCompteBeneficiaire("myNr");
        Versement versement = transactionMapper.map(versementDto);
        assertEquals(versement.getNomPrenomEmetteur(), versementDto.getNomPrenomEmetteur());
        assertEquals(versement.getCompteBeneficiaire().getNrCompte(), versementDto.getNrCompteBeneficiaire());
        assertEquals(versement.getDateExecution(), versementDto.getDateExecution());
        assertEquals(versement.getMontant(), versementDto.getMontant());
        assertEquals(versement.getMotif(), versementDto.getMotif());
    }
}
