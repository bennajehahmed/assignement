package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.CompteDto;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CompteMapperTest {
    CompteMapper compteMapper = new CompteMapper();

    @Test
    public void map_shouldReturnNullWhenGivenNull() {
        assertNull(compteMapper.map(null));
    }

    @Test
    public void mapToCompteDto_ok() {
        Compte compte = createCompte();
        CompteDto compteDto = compteMapper.map(compte);
        assertEquals(compte.getNrCompte(), compteDto.getNrCompte());
        assertEquals(compte.getSolde(), compteDto.getSolde());
        assertEquals(compte.getRib(), compteDto.getRib());
    }

    private Compte createCompte() {
        Compte compte = new Compte();
        compte.setNrCompte("numero");
        compte.setSolde(new BigDecimal(10000));
        compte.setRib("RIBABCAAZERT");
        compte.setUtilisateur(new Utilisateur());
        return compte;
    }
}
