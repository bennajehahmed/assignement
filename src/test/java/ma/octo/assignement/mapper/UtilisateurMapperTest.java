package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Date;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class UtilisateurMapperTest {
    CompteMapper compteMapper = Mockito.mock(CompteMapper.class);
    UtilisateurMapper utilisateurMapper = new UtilisateurMapper(compteMapper);

    @BeforeEach
    public void setup(){
        when(compteMapper.map(any())).thenReturn(null);
    }

    @Test
    public void map_shouldReturnNullWhenGivenNull() {
        assertNull(utilisateurMapper.map(null));
    }

    @Test
    public void mapToUtilisateurDto_ok() {
        Utilisateur utilisateur = createUtilisateur();
        UtilisateurDto utilisateurDto = utilisateurMapper.map(utilisateur);

        assertEquals(utilisateur.getBirthdate(), utilisateurDto.getBirthdate());
        assertEquals(utilisateur.getFirstname(), utilisateurDto.getFirstname());
        assertEquals(utilisateur.getLastname(), utilisateurDto.getLastname());
        assertEquals(utilisateur.getGender(), utilisateurDto.getGender());
        assertEquals(utilisateur.getUsername(), utilisateurDto.getUsername());
        assertEquals(utilisateur.getComptes().size(), utilisateurDto.getComptes().size());
    }

    public Utilisateur createUtilisateur() {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setBirthdate(new Date());
        utilisateur.setFirstname("firstName");
        utilisateur.setLastname("lastName");
        utilisateur.setGender("male");
        utilisateur.setUsername("user1");
        utilisateur.setComptes(new HashSet<>());
        return utilisateur;
    }
}
