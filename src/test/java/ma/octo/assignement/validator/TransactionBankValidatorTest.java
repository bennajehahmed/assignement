package ma.octo.assignement.validator;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VersementBankDto;
import ma.octo.assignement.dto.VirementBankDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class TransactionBankValidatorTest {

    CompteRepository compteRepository = Mockito.mock(CompteRepository.class);
    TransactionValidator transactionValidator = new TransactionValidator(compteRepository);
    Compte compteBeneficiare = new Compte();
    Compte compteEmetteur = new Compte();

    @BeforeEach
    public void setUp() {
        compteBeneficiare.setSolde(new BigDecimal(2000));
        compteEmetteur.setSolde(new BigDecimal(2000));
        when(compteRepository.findByNrCompte(eq("beneficiaire"))).thenReturn(compteBeneficiare);
        when(compteRepository.findByNrCompte(eq("emetteur"))).thenReturn(compteEmetteur);
        when(compteRepository.findByNrCompte(eq("does not exist"))).thenReturn(null);
    }

    @Test
    public void virementValidator_NotOk() {
        assertThrows(TransactionException.class, () -> transactionValidator.validateVirement(null), "TransactionBank validator should throw exception if given null");
        assertThrows(CompteNonExistantException.class, () ->
                transactionValidator.validateVirement(createVirement(new Date(), "emetteur", null, new BigDecimal(1239), "Achat")), "TransactionBank validator should throw exception if nbr compte Emetteur is null");
        assertThrows(CompteNonExistantException.class, () ->
                transactionValidator.validateVirement(createVirement(new Date(), "emetteur", "does not exist", new BigDecimal(1239), "Achat")), "TransactionBank validator should throw exception if compte Emetteur does not exists");
        assertThrows(TransactionException.class, () ->
                transactionValidator.validateVirement(createVirement(new Date(), "emetteur", "beneficiaire", new BigDecimal(0), "Achat")), "TransactionBank validator should throw exception if montant is too small");
        assertThrows(TransactionException.class, () ->
                transactionValidator.validateVirement(createVirement(new Date(), "emetteur", "beneficiaire", new BigDecimal(999999999), "Achat")), "TransactionBank validator should throw exception if montant is too big");
        assertThrows(TransactionException.class, () ->
                transactionValidator.validateVirement(createVirement(new Date(), "emetteur", "beneficiaire", null, "Achat")), "TransactionBank validator should throw exception if montant is null");
        assertThrows(CompteNonExistantException.class, () ->
                transactionValidator.validateVirement(createVirement(new Date(), null, "beneficiaire", new BigDecimal(1239), "Achat")), "TransactionBank validator should throw exception if nbr Beneficiare is null");
        assertThrows(CompteNonExistantException.class, () ->
                transactionValidator.validateVirement(createVirement(new Date(), "does not exist", "beneficiaire", new BigDecimal(1239), "Achat")), "TransactionBank validator should throw exception if compte Beneficiare does not exist");
        assertThrows(TransactionException.class, () ->
                transactionValidator.validateVirement(createVirement(null, "emetteur", "beneficiaire", new BigDecimal(1239), "Achat")), "TransactionBank validator should throw exception if date is null");
        assertThrows(TransactionException.class, () ->
                transactionValidator.validateVirement(createVirement(new Date(), "emetteur", "beneficiaire", new BigDecimal(1239), null)), "TransactionBank validator should throw exception if date is null");
        assertThrows(SoldeDisponibleInsuffisantException.class, () ->
                transactionValidator.validateVirement(createVirement(new Date(), "emetteur", "beneficiaire", new BigDecimal(3000), "motif")), "TransactionBank validator should throw exception if solde is insufficient");
    }

    @Test
    public void virementValidator_Ok() throws TransactionException, SoldeDisponibleInsuffisantException, CompteNonExistantException {
        transactionValidator.validateVirement(createVirement(new Date(), "emetteur", "beneficiaire", new BigDecimal(500), "motif"));
        verify(compteRepository, times(3)).findByNrCompte(any());
    }

    @Test
    public void versementValidator_NotOk() {
        VersementBankDto versementDto = new VersementBankDto();
        versementDto.setNomPrenomEmetteur("");
        versementDto.setMontant(new BigDecimal(2000));
        versementDto.setDateExecution(new Date());
        versementDto.setNrCompteBeneficiaire("beneficiaire");
        versementDto.setMotif("motif");
        assertThrows(TransactionException.class, () ->
                transactionValidator.validateVersement(versementDto), "TransactionBank validator should throw exception if nomPrenom is empty");

    }

    @Test
    public void versementValidator_Ok() throws TransactionException, CompteNonExistantException {
        VersementBankDto versementDto = new VersementBankDto();
        versementDto.setNomPrenomEmetteur("flhlk");
        versementDto.setMontant(new BigDecimal(2000));
        versementDto.setDateExecution(new Date());
        versementDto.setNrCompteBeneficiaire("beneficiaire");
        versementDto.setMotif("motif");
        transactionValidator.validateVersement(versementDto);
        verify(compteRepository, times(1)).findByNrCompte(any());
    }


    private VirementBankDto createVirement(Date date, String compteE, String compteB, BigDecimal montant, String motif) {
        VirementBankDto virementDto = new VirementBankDto();

        virementDto.setDateExecution(date);
        virementDto.setNrCompteBeneficiaire(compteB);
        virementDto.setNrCompteEmetteur(compteE);
        virementDto.setMontant(montant);
        virementDto.setMotif(motif);

        return virementDto;
    }
}
