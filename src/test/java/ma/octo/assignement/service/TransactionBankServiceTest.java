package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VersementBankDto;
import ma.octo.assignement.dto.VirementBankDto;
import ma.octo.assignement.mapper.TransactionMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class TransactionBankServiceTest {

    CompteRepository compteRepository = Mockito.mock(CompteRepository.class);
    VirementRepository virementRepository = Mockito.mock(VirementRepository.class);
    VersementRepository versementRepository = Mockito.mock(VersementRepository.class);
    TransactionMapper transactionMapper = Mockito.mock(TransactionMapper.class);
    TransactionService transactionService = new TransactionService(compteRepository, virementRepository, versementRepository, transactionMapper);
    Compte compteEmetteur = new Compte();
    Compte compteBeneficiaire = new Compte();

    @BeforeEach
    public void setUp() {
        compteEmetteur.setSolde(new BigDecimal(50000));
        compteBeneficiaire.setSolde(new BigDecimal(20000));
        when(compteRepository.findByNrCompte(eq("emetteur"))).thenReturn(compteEmetteur);
        when(compteRepository.findByNrCompte(eq("beneficiaire"))).thenReturn(compteBeneficiaire);
    }

    @Test
    public void createVirement_ok() {
        transactionService.createVirement(createVirementDto());
        assertEquals(compteEmetteur.getSolde().intValue(), 50000 - 10000);
        assertEquals(compteBeneficiaire.getSolde().intValue(), 20000 + 10000);
        verify(compteRepository, times(2)).save(any());
        verify(virementRepository, times(1)).save(any());
        verify(transactionMapper, times(1)).map((VirementBankDto) any());
    }

    @Test
    public void createVersement_ok() {
        transactionService.createVersement(createVersementDto());
        assertEquals(compteBeneficiaire.getSolde().intValue(), 20000 + 10000);
        verify(compteRepository, times(1)).save(any());
        verify(versementRepository, times(1)).save(any());
        verify(transactionMapper, times(1)).map((VersementBankDto) any());
    }

    VirementBankDto createVirementDto() {
        VirementBankDto virementDto = new VirementBankDto();
        virementDto.setNrCompteBeneficiaire("beneficiaire");
        virementDto.setNrCompteEmetteur("emetteur");
        virementDto.setMotif("motif");
        virementDto.setDateExecution(new Date());
        virementDto.setMontant(new BigDecimal(10000));
        return virementDto;
    }

    VersementBankDto createVersementDto() {
        VersementBankDto versementDto = new VersementBankDto();
        versementDto.setNrCompteBeneficiaire("beneficiaire");
        versementDto.setNomPrenomEmetteur("Mr emetteur");
        versementDto.setMotif("motif");
        versementDto.setDateExecution(new Date());
        versementDto.setMontant(new BigDecimal(10000));
        return versementDto;
    }
}
