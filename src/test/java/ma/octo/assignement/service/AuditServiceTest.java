package ma.octo.assignement.service;

import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditTransactionRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class AuditServiceTest {
    AuditTransactionRepository auditTransactionRepository = Mockito.mock(AuditTransactionRepository.class);
    AuditService auditService = new AuditService(auditTransactionRepository);

    @Test
    public void auditVirement_ok() {
        auditService.auditTransaction("message", EventType.VIREMENT);
        verify(auditTransactionRepository, times(1)).save(any());
    }

    @Test
    public void auditVersement() {
        auditService.auditTransaction("message", EventType.VERSEMENT);
        verify(auditTransactionRepository, times(1)).save(any());
    }

}
