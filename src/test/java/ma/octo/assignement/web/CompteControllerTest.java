package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class CompteControllerTest {
    private MockMvc mockMvc;
    @Mock
    private CompteRepository compteRepository;
    @Mock
    private CompteMapper compteMapper;
    @InjectMocks
    private CompteController compteController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(compteController)
                .build();
    }

    @Test
    public void getComptes_ok() throws Exception {
        List<Compte> comptes = new ArrayList<>();
        comptes.add(null);
        comptes.add(null);
        comptes.add(null);
        when(compteRepository.findAll()).thenReturn(comptes);
        when(compteMapper.map(any())).thenReturn(createCompteDto());
        ResultActions ra = mockMvc.perform(
                get("/api/v1/comptes"));
        ra.andExpect(status().is(HttpServletResponse.SC_OK));
        ra.andExpect(jsonPath("$").isArray());
        ra.andExpect(jsonPath("$[0].nrCompte").value("Nr compte"));
        ra.andExpect(jsonPath("$[0].rib").value("riiiiiiiiiiiiiib"));
        ra.andExpect(jsonPath("$[0].solde").value(10000));


    }

    private CompteDto createCompteDto() {
        CompteDto compteDto = new CompteDto();
        compteDto.setSolde(new BigDecimal(10000));
        compteDto.setRib("riiiiiiiiiiiiiib");
        compteDto.setNrCompte("Nr compte");
        return compteDto;
    }

}
