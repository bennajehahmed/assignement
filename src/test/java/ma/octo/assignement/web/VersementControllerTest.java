package ma.octo.assignement.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementBankDto;
import ma.octo.assignement.mapper.TransactionMapper;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransactionService;
import ma.octo.assignement.validator.TransactionValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class VersementControllerTest {
    @Mock
    VersementRepository versementRepository;
    @Mock
    AuditService auditService;
    @Mock
    TransactionValidator transactionValidator;
    @Mock
    TransactionService transactionService;
    @Mock
    TransactionMapper transactionMapper;
    @InjectMocks
    VersementController versementController;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(versementController)
                .build();
    }

    @Test
    public void createVersement_ok() throws Exception {
        VersementBankDto versementDto = createVersementDto();
        doNothing().when(transactionValidator).validateVersement(eq(versementDto));
        doNothing().when(transactionService)
                .createVersement(eq(versementDto));
        doNothing().when(auditService)
                .auditTransaction(any(), eq(EventType.VERSEMENT));
        ResultActions ra = mockMvc.perform(
                post("/api/v1/versements")
                        .contentType(APPLICATION_JSON)
                        .content(objectAsJsonString(versementDto)));
        ra.andExpect(status().is(HttpServletResponse.SC_CREATED));
    }

    @Test
    public void getVersements_ok() throws Exception {
        List<Versement> versements = new ArrayList<>();
        versements.add(null);
        versements.add(null);
        versements.add(null);
        when(versementRepository.findAll()).thenReturn(versements);
        when(transactionMapper.map((Versement) any())).thenReturn(createVersementDto());
        ResultActions ra = mockMvc.perform(
                get("/api/v1/versements"));
        ra.andExpect(status().is(HttpServletResponse.SC_OK));
        ra.andExpect(jsonPath("$").isArray());
        ra.andExpect(jsonPath("$[0].montant").value(20000));
        ra.andExpect(jsonPath("$[0].motif").value("Motif"));
        ra.andExpect(jsonPath("$[0].nrCompteBeneficiaire").value("Nr Beneficiare"));
        ra.andExpect(jsonPath("$[0].nomPrenomEmetteur").value("Mr Emetteur"));
        ra.andExpect(jsonPath("$[0].dateExecution").value("1394496000000"));


    }

    VersementBankDto createVersementDto() {
        VersementBankDto versementDto = new VersementBankDto();
        versementDto.setMontant(new BigDecimal(20000));
        Calendar myCalendar = new GregorianCalendar(2014, Calendar.MARCH, 11);
        Date myDate = myCalendar.getTime();
        versementDto.setDateExecution(myDate);
        versementDto.setMotif("Motif");
        versementDto.setNomPrenomEmetteur("Mr Emetteur");
        versementDto.setNrCompteBeneficiaire("Nr Beneficiare");
        return versementDto;
    }

    public static String objectAsJsonString(final Object obj) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
