package ma.octo.assignement.web;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class UtilisateurControllerTest {
    private MockMvc mockMvc;
    @Mock
    private UtilisateurRepository utilisateurRepository;
    @Mock
    private UtilisateurMapper utilisateurMapper;
    @InjectMocks
    private UtilisateurController utilisateurController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(utilisateurController)
                .build();
    }

    @Test
    public void getUtilisateurs_ok() throws Exception {
        List<Utilisateur> utilisateurs = new ArrayList<>();
        utilisateurs.add(null);
        utilisateurs.add(null);
        utilisateurs.add(null);
        when(utilisateurRepository.findAll()).thenReturn(utilisateurs);
        when(utilisateurMapper.map(any())).thenReturn(createUtilisateurDto());
        ResultActions ra = mockMvc.perform(
                get("/api/v1/utilisateurs"));
        ra.andExpect(status().is(HttpServletResponse.SC_OK));
        ra.andExpect(jsonPath("$").isArray());
        ra.andExpect(jsonPath("$[0].firstname").value("first name"));
        ra.andExpect(jsonPath("$[0].lastname").value("last name"));
        ra.andExpect(jsonPath("$[0].username").value("username"));
        ra.andExpect(jsonPath("$[0].gender").value("gender"));
        ra.andExpect(jsonPath("$[0].birthdate").value("1394496000000"));
        ra.andExpect(jsonPath("$[0].comptes").isArray());

    }

    private UtilisateurDto createUtilisateurDto() {
        UtilisateurDto utilisateurDto = new UtilisateurDto();

        utilisateurDto.setFirstname("first name");
        utilisateurDto.setLastname("last name");
        utilisateurDto.setUsername("username");
        utilisateurDto.setGender("gender");
        Calendar myCalendar = new GregorianCalendar(2014, Calendar.MARCH, 11);
        Date birthDate = myCalendar.getTime();
        utilisateurDto.setBirthdate(birthDate);
        utilisateurDto.setComptes(new HashSet<>());
        return utilisateurDto;

    }


}
