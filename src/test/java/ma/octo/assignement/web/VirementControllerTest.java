package ma.octo.assignement.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementBankDto;
import ma.octo.assignement.mapper.TransactionMapper;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransactionService;
import ma.octo.assignement.validator.TransactionValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class VirementControllerTest {

    @Mock
    VirementRepository virementRepository;
    @Mock
    AuditService auditService;
    @Mock
    TransactionValidator transactionValidator;
    @Mock
    TransactionService transactionService;
    @Mock
    TransactionMapper transactionMapper;
    @InjectMocks
    VirementController virementController;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(virementController)
                .build();
    }

    @Test
    public void createVirement_ok() throws Exception {
        VirementBankDto virementDto = createVirementDto();
        doNothing().when(transactionValidator).validateVirement(eq(virementDto));
        doNothing().when(transactionService)
                .createVirement(eq(virementDto));
        doNothing().when(auditService)
                .auditTransaction(any(), eq(EventType.VIREMENT));
        ResultActions ra = mockMvc.perform(
                post("/api/v1/virements")
                        .contentType(APPLICATION_JSON)
                        .content(objectAsJsonString(virementDto)));
        ra.andExpect(status().is(HttpServletResponse.SC_CREATED));
    }

    @Test
    public void getVirements_ok() throws Exception {
        List<Virement> virements = new ArrayList<>();
        virements.add(null);
        virements.add(null);
        virements.add(null);
        when(virementRepository.findAll()).thenReturn(virements);
        when(transactionMapper.map((Virement) any())).thenReturn(createVirementDto());
        ResultActions ra = mockMvc.perform(
                get("/api/v1/virements"));
        ra.andExpect(status().is(HttpServletResponse.SC_OK));
        ra.andExpect(jsonPath("$").isArray());
        ra.andExpect(jsonPath("$[0].montant").value(20000));
        ra.andExpect(jsonPath("$[0].motif").value("Motif"));
        ra.andExpect(jsonPath("$[0].nrCompteBeneficiaire").value("Nr Beneficiare"));
        ra.andExpect(jsonPath("$[0].nrCompteEmetteur").value("Nr Emetteur"));
        ra.andExpect(jsonPath("$[0].dateExecution").value("1394496000000"));


    }

    VirementBankDto createVirementDto() {
        VirementBankDto virementDto = new VirementBankDto();
        virementDto.setMontant(new BigDecimal(20000));
        Calendar myCalendar = new GregorianCalendar(2014, Calendar.MARCH, 11);
        Date myDate = myCalendar.getTime();
        virementDto.setDateExecution(myDate);
        virementDto.setMotif("Motif");
        virementDto.setNrCompteEmetteur("Nr Emetteur");
        virementDto.setNrCompteBeneficiaire("Nr Beneficiare");
        return virementDto;
    }

    public static String objectAsJsonString(final Object obj) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
