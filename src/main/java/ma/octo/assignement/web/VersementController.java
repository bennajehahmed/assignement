package ma.octo.assignement.web;

import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementBankDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransactionMapper;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransactionService;
import ma.octo.assignement.validator.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController()
@RequestMapping(value = "api/v1")
public class VersementController {
    private final VersementRepository versementRepository;
    private final TransactionValidator transactionValidator;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final AuditService auditService;

    @Autowired
    public VersementController(VersementRepository versementRepository, TransactionValidator transactionValidator, TransactionService transactionService, TransactionMapper transactionMapper, AuditService auditService) {
        this.versementRepository = versementRepository;
        this.transactionValidator = transactionValidator;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.auditService = auditService;
    }


    @GetMapping("versements")
    Set<VersementBankDto> allVersement() {
        Set<VersementBankDto> versementDtos = new HashSet<>();
        versementRepository.findAll().forEach(versement -> {
            VersementBankDto versementDto = transactionMapper.map(versement);
            versementDtos.add(versementDto);
        });
        return versementDtos;
    }

    @PostMapping("/versements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVersement(@RequestBody VersementBankDto versementDto)
            throws CompteNonExistantException, TransactionException {

        transactionValidator.validateVersement(versementDto);
        transactionService.createVersement(versementDto);
        auditService.auditTransaction("Versement depuis " + versementDto.getNomPrenomEmetteur() + " vers " + versementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontant()
                .toString(), EventType.VERSEMENT);
    }

}
