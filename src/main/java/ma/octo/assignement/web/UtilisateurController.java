package ma.octo.assignement.web;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController()
@RequestMapping(value = "api/v1")
public class UtilisateurController {
    private final UtilisateurRepository utilisateurRepository;
    private final UtilisateurMapper utilisateurMapper;

    @Autowired
    public UtilisateurController(UtilisateurRepository utilisateurRepository, UtilisateurMapper utilisateurMapper) {
        this.utilisateurRepository = utilisateurRepository;
        this.utilisateurMapper = utilisateurMapper;
    }

    @GetMapping("utilisateurs")
    Set<UtilisateurDto> getAllUtilisateur() {
        Set<UtilisateurDto> utilisateurDtos = new HashSet<>();
        utilisateurRepository.findAll().forEach(utilisateur ->
                utilisateurDtos.add(utilisateurMapper.map(utilisateur))
        );
        return utilisateurDtos;
    }
}
