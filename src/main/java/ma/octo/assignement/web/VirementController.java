package ma.octo.assignement.web;

import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementBankDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransactionMapper;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransactionService;
import ma.octo.assignement.validator.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController()
@RequestMapping(value = "api/v1")

public class VirementController {

    // Logger LOGGER = LoggerFactory.getLogger(VirementController.class);


    private final VirementRepository virementRepository;
    private final AuditService auditService;
    private final TransactionValidator transactionValidator;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;

    @Autowired
    public VirementController(VirementRepository virementRepository, AuditService auditService, TransactionValidator transactionValidator, TransactionService transactionService, TransactionMapper transactionMapper) {
        this.virementRepository = virementRepository;
        this.auditService = auditService;
        this.transactionValidator = transactionValidator;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
    }


    @GetMapping("virements")
    @ResponseStatus(HttpStatus.OK)
    Set<VirementBankDto> getVirements() {
        Set<VirementBankDto> virementDtos = new HashSet<>();
        virementRepository.findAll().forEach(virement -> {
            VirementBankDto virementDto = transactionMapper.map(virement);
            virementDtos.add(virementDto);
        });
        return virementDtos;
    }


    @PostMapping("/virements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVirement(@RequestBody VirementBankDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        transactionValidator.validateVirement(virementDto);
        transactionService.createVirement(virementDto);
        auditService.auditTransaction("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontant()
                .toString(), EventType.VIREMENT);
    }


}
