package ma.octo.assignement.web;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController()
@RequestMapping(value = "api/v1")
public class CompteController {

    private final CompteRepository compteRepository;
    private final CompteMapper compteMapper;

    @Autowired
    public CompteController(CompteRepository compteRepository, CompteMapper compteMapper) {
        this.compteRepository = compteRepository;
        this.compteMapper = compteMapper;
    }

    @GetMapping("comptes")
    @ResponseStatus(HttpStatus.OK)
    Set<CompteDto> getAllComptes() {
        Set<CompteDto> compteDtos = new HashSet<>();
        compteRepository.findAll().forEach(compte ->
                compteDtos.add(compteMapper.map(compte))
        );
        return compteDtos;
    }
}
