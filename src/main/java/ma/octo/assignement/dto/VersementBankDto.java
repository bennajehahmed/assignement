package ma.octo.assignement.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class VersementBankDto extends TransactionBankDto {
    private String nomPrenomEmetteur;
}
