package ma.octo.assignement.dto;

import lombok.Data;

import java.util.Date;
import java.util.Set;

@Data
public class UtilisateurDto {

    private String username;
    private String gender;
    private String lastname;
    private String firstname;
    private Date birthdate;
    private Set<CompteDto> comptes;

}
