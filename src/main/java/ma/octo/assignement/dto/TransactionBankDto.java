package ma.octo.assignement.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public abstract class TransactionBankDto {

    private BigDecimal montant;
    private Date dateExecution;
    private String nrCompteBeneficiaire;
    private String motif;
}
