package ma.octo.assignement.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
public class VirementBankDto extends TransactionBankDto {
  private String nrCompteEmetteur;
}
