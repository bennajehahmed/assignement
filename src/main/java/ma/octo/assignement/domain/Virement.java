package ma.octo.assignement.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "VIREMENT")
@Data
@EqualsAndHashCode(callSuper = true)
public class Virement extends TransactionBank implements Serializable {

  @ManyToOne
  private Compte compteEmetteur;

}
