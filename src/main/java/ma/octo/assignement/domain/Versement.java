package ma.octo.assignement.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "VERSEMENT")
@Data
@EqualsAndHashCode(callSuper = true)
public class Versement extends TransactionBank implements Serializable {

  @Column
  private String nomPrenomEmetteur;





}
