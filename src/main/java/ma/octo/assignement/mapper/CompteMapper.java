package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import org.springframework.stereotype.Component;

@Component
public class CompteMapper {

    public CompteDto map(Compte compte) {
        if (compte == null) return null;
        CompteDto compteDto = new CompteDto();
        compteDto.setNrCompte(compte.getNrCompte());
        compteDto.setRib(compte.getRib());
        compteDto.setSolde(compte.getSolde());
        //compteDto.setUtilisateur(compte.getUtilisateur());
        return compteDto;
    }

}
