package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementBankDto;
import ma.octo.assignement.dto.VirementBankDto;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {
    private final CompteRepository compteRepository;

    @Autowired
    public TransactionMapper(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }

    public VirementBankDto map(Virement virement) {
        if (virement == null) return null;
        VirementBankDto virementDto = new VirementBankDto();

        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur() == null ? null : virement.getCompteEmetteur().getNrCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire() == null ? null : virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setMontant(virement.getMontant());
        virementDto.setDateExecution(virement.getDateExecution());
        virementDto.setMotif(virement.getMotif());

        return virementDto;
    }

    public Virement map(VirementBankDto virementDto) {
        if (virementDto == null) return null;
        Virement virement = new Virement();
        virement.setCompteEmetteur(compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur()));
        virement.setCompteBeneficiaire(compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire()));
        virement.setMontant(virementDto.getMontant());
        virement.setDateExecution(virementDto.getDateExecution());
        virement.setMotif(virementDto.getMotif());
        return virement;
    }

    public VersementBankDto map(Versement versement) {
        if (versement == null) return null;
        VersementBankDto versementDto = new VersementBankDto();

        versementDto.setNomPrenomEmetteur(versement.getNomPrenomEmetteur());
        versementDto.setNrCompteBeneficiaire(versement.getCompteBeneficiaire() == null ? null : versement.getCompteBeneficiaire().getNrCompte());
        versementDto.setMontant(versement.getMontant());
        versementDto.setDateExecution(versement.getDateExecution());
        versementDto.setMotif(versement.getMotif());

        return versementDto;
    }

    public Versement map(VersementBankDto versementDto) {
        if (versementDto == null) return null;
        Versement versement = new Versement();
        versement.setNomPrenomEmetteur(versementDto.getNomPrenomEmetteur());
        versement.setCompteBeneficiaire(compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire()));
        versement.setMontant(versementDto.getMontant());
        versement.setDateExecution(versementDto.getDateExecution());
        versement.setMotif(versementDto.getMotif());
        return versement;
    }

}
