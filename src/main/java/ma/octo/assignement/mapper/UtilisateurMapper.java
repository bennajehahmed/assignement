package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class UtilisateurMapper {

    private final CompteMapper compteMapper;

    public UtilisateurMapper(CompteMapper compteMapper) {
        this.compteMapper = compteMapper;
    }

    public UtilisateurDto map(Utilisateur utilisateur){
        if(utilisateur==null)return null;
        UtilisateurDto utilisateurDto=new UtilisateurDto();

        utilisateurDto.setFirstname(utilisateur.getFirstname());
        utilisateurDto.setLastname(utilisateur.getLastname());
        utilisateurDto.setBirthdate(utilisateur.getBirthdate());
        utilisateurDto.setGender(utilisateur.getGender());
        utilisateurDto.setUsername(utilisateur.getUsername());
        utilisateurDto.setComptes(utilisateur.getComptes().stream()
                .map(compteMapper::map).collect(Collectors.toSet()));
        return utilisateurDto;

    }
}
