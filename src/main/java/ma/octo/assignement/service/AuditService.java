package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditTransaction;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditTransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditService {

    private final AuditTransactionRepository auditTransactionRepository;
    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    public AuditService(AuditTransactionRepository auditTransactionRepository) {
        this.auditTransactionRepository = auditTransactionRepository;
    }

    public void auditTransaction(String message,EventType eventType) {

        LOGGER.info("Audit de l'événement {}", eventType);

        AuditTransaction audit = new AuditTransaction();
        audit.setEventType(eventType);
        audit.setMessage(message);
        auditTransactionRepository.save(audit);
    }


}
