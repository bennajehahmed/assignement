package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VersementBankDto;
import ma.octo.assignement.dto.VirementBankDto;
import ma.octo.assignement.mapper.TransactionMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Transactional
public class TransactionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionService.class);

    private final CompteRepository compteRepository;
    private final VirementRepository virementRepository;
    private final VersementRepository versementRepository;
    private final TransactionMapper transactionMapper;

    @Autowired
    public TransactionService(CompteRepository compteRepository, VirementRepository virementRepository, VersementRepository versementRepository, TransactionMapper transactionMapper) {
        this.compteRepository = compteRepository;
        this.virementRepository = virementRepository;
        this.versementRepository = versementRepository;
        this.transactionMapper = transactionMapper;
    }

    public void createVirement(VirementBankDto virementDto) {
        LOGGER.info("Réception du virement {}", virementDto);
        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontant()));
        compteRepository.save(compteEmetteur);
        compteBeneficiaire.setSolde(BigDecimal.valueOf(compteBeneficiaire.getSolde().doubleValue()
                + virementDto.getMontant().doubleValue()));
        compteRepository.save(compteBeneficiaire);
        virementRepository.save(transactionMapper.map(virementDto));

    }

    public void createVersement(VersementBankDto versementDto) {
        LOGGER.info("Réception du versement {}", versementDto);
        Compte compteBeneficiaire = compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());

        compteBeneficiaire.setSolde(BigDecimal.valueOf(compteBeneficiaire.getSolde().doubleValue()
                + versementDto.getMontant().doubleValue()));
        compteRepository.save(compteBeneficiaire);
        versementRepository.save(transactionMapper.map(versementDto));

    }
}
