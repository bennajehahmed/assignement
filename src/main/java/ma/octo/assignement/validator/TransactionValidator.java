package ma.octo.assignement.validator;

import ma.octo.assignement.dto.TransactionBankDto;
import ma.octo.assignement.dto.VersementBankDto;
import ma.octo.assignement.dto.VirementBankDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class TransactionValidator {
    private static final int MONTANT_MAXIMAL = 10000;
    private static final int MONTANT_MINIMAL = 10;
    private final CompteRepository compteRepository;

    @Autowired
    public TransactionValidator(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }

    public void validateVirement(VirementBankDto virementDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        validateCommonData(virementDto);
        validateVirementData(virementDto);
    }

    public void validateVersement(VersementBankDto versementDto) throws TransactionException, CompteNonExistantException {
        validateCommonData(versementDto);
        validateVersementData(versementDto);
    }

    private void validateCommonData(TransactionBankDto transactionBankDto) throws TransactionException, CompteNonExistantException {
        if (transactionBankDto == null) {
            throw new TransactionException("transaction null");
        }
        if (!StringUtils.hasText(transactionBankDto.getNrCompteBeneficiaire())) {
            throw new CompteNonExistantException("compte beneficiaire non mentionné");
        }
        if (compteRepository.findByNrCompte(transactionBankDto.getNrCompteBeneficiaire()) == null) {
            throw new CompteNonExistantException("compte beneficiaire non existant");
        }
        if (!StringUtils.hasText(transactionBankDto.getMotif())) {
            throw new TransactionException("motif non mentionné");
        }
        if (transactionBankDto.getDateExecution() == null) {
            throw new TransactionException("date non mentionné");
        }
        if (transactionBankDto.getMontant() == null) {
            throw new TransactionException("Montant vide");
        }
        if (transactionBankDto.getMontant().doubleValue() < MONTANT_MINIMAL) {
            throw new TransactionException("Montant minimal de virement non atteint");
        }
        if (transactionBankDto.getMontant().doubleValue() > MONTANT_MAXIMAL) {
            throw new TransactionException("Montant maximal de virement dépassé");
        }

    }

    private void validateVirementData(VirementBankDto virementDto) throws CompteNonExistantException, SoldeDisponibleInsuffisantException {
        if (!StringUtils.hasText(virementDto.getNrCompteEmetteur())) {
            throw new CompteNonExistantException("compte emetteur non mentionné");
        }
        if (compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur()) == null) {
            throw new CompteNonExistantException("compte emetteur non existant");
        }
        if (compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur())
                .getSolde().intValue() - virementDto.getMontant().intValue() < 0) {
            throw new SoldeDisponibleInsuffisantException("solde insuffisant");
        }

    }

    private void validateVersementData(VersementBankDto versementDto) throws TransactionException {
        if (!StringUtils.hasText(versementDto.getNomPrenomEmetteur())) {
            throw new TransactionException("nom et prenom de l'emetteur non mentionné");
        }
    }

}
